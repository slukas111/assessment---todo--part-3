import todosList from "./todos.json";
import { TOGGLE_TODO, DELETE_TODO, CLEAR_COMPLETED_TODOS, ADD_TODO } from "./actions";

const initialState = { //#2
  todos: todosList, 
  value: ""

};

const reducer = (state = initialState, action) => { //start reducer function how to modify state #1
  switch (action.type) {
    case TOGGLE_TODO: {
      const newTodoList = state.todos.map(todo => {
        if (todo.id === action.payload) {   //conditional logic
          const newTodo = { ...todo };
          newTodo.completed = !newTodo.completed;
          return newTodo;
        }
        return todo;
      });
      return { todos: newTodoList };
    }
    
    case ADD_TODO:{
      return { ...state, todos: [...state.todos, action.payload] };}
      default:
        return state;
        case DELETE_TODO: {
          const newTodoList = state.todos.filter(
            todo => todo.id !== action.payload )
            return {...state, todos: newTodoList};
          }
            case CLEAR_COMPLETED_TODOS: {
            const completed = state.todos.filter(
              todo => todo.completed === false);
    return {...state, todos: completed}
    }
  }
};

export default reducer;